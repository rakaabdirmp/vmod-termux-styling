#!/data/data/com.termux/files/usr/bin/bash
# Vars
if [ -n "${TERMUX_VERSION}" ]; then
	T_APP_VERSION="\nv${TERMUX_VERSION}"
elif [ -n "${TERMUX_APP__VERSION_NAME}" ]; then
	T_APP_VERSION="\nv${TERMUX_APP__VERSION_NAME}"
fi
if [ -n "${TERMUX_APK_RELEASE}" ]; then
	T_APK_RELEASE="\e[0;34m|\033[0m ${TERMUX_APK_RELEASE}"
elif [ -n "${TERMUX_APP__APK_RELEASE}" ]; then
	T_APK_RELEASE="\e[0;34m|\033[0m ${TERMUX_APP__APK_RELEASE}"
fi
if [ -n "${T_APP_VERSION}" ] || [ -n "${T_APK_RELEASE}" ]; then
	nl="\n"
fi
# Welcome message
echo -e "\e[0;34m$(toilet -t -f smslant -F crop Vmod)\033[0m

\e[0;34m${T_APP_VERSION} Mod And Style By Zyarexx/rakarmp\033[0m

\e[0;34mWorking with packages\033[0m:
  Cari:    pkg search <query>
  Install: pkg install <package>
  Update:  pkg update

\e[0;34mSubscribing to additional repos\033[0m:
  Root:    pkg install root-repo
  X11:     pkg install x11-repo

\e[0;34mUntuk memperbaiki masalah repositori\033[0m:
  Coba gunakan perintah \e[4mtermux-change-repo\e[0m\n"
